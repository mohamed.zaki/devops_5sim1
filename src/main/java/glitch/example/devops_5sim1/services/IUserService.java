package glitch.example.devops_5sim1.services;

import glitch.example.devops_5sim1.entities.User;

import java.util.List;

public interface IUserService { 
	 
	List<User> retrieveAllUsers();
	User addUser(User u);
	void deleteUser(String id);
	User updateUser(User u);
	User retrieveUser(String id);

} 