package glitch.example.devops_5sim1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Devops5sim1Application {

    public static void main(String[] args) {
        SpringApplication.run(Devops5sim1Application.class, args);
    }

    @GetMapping("/")
    public String hello()
    {
        return "Hello From SpringBoot";
    }

}
