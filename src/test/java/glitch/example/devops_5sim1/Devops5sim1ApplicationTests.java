package glitch.example.devops_5sim1;

import glitch.example.devops_5sim1.entities.Role;
import glitch.example.devops_5sim1.entities.User;
import glitch.example.devops_5sim1.services.IUserService;
import glitch.example.devops_5sim1.services.UserServiceImpl;
import net.minidev.json.parser.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.SimpleDateFormat;
import java.util.Date;

@SpringBootTest
class Devops5sim1ApplicationTests {

    @Autowired
    IUserService us;

    private static final Logger l = LogManager.getLogger(UserServiceImpl.class);


    @Test
    public void testAddUser() throws ParseException, java.text.ParseException {
        l.info("Test Add User Message");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date d = dateFormat.parse("1996-11-12");
        User u = new User("test" , "personne2 " , d , Role.CHEF_DEPARTEMENT);
        User userAdded = us.addUser(u);
        Assertions.assertEquals(u.getLastName(), userAdded.getLastName());
    }

//    @Test
//	public  void retrieveAllUseres() {
//		List<User> users = us.retrieveAllUsers();
//		int i=0;
//		for (User user : users){
//			l.debug("user ++++ : "+user.getLastName());
//			i++;
//		}
//		l.info("count users "+i);
//		Assertions.assertEquals(i, ((List<?>) users).size());
//    }

}
