FROM openjdk:11
COPY target/devops_5sim1-0.0.1-SNAPSHOT.jar devops_5sim1-0.0.1-SNAPSHOT.jar
EXPOSE 3000
ENTRYPOINT ["java","-jar","/devops_5sim1-0.0.1-SNAPSHOT.jar"]